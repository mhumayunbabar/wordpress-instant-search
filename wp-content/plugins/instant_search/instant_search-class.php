<?php 

function add_instant_search(){

	add_action('wp_footer', 'is_add_scripts', 5);

	$html = ' <header class="row">
		    <div class="searchbox-container">
		    <lable>Search here: </lable>
		      <div class="input-group">
		        <input type="text" class="form-control" id="q" />
		        <span class="input-group-btn">
		          <button class="btn btn-default"><i class="fa fa-search"></i></button>
		        </span>
		      </div>
		    </div>
		  </header>
		  <section class="">
		    <article>
		      <div id="stats" class="text-right text-muted"></div>
		      <hr />
		      <div id="hits"></div>
		      <div id="pagination" class="text-center"></div>
		    </article>
		  </section>';

		  return $html;
}


function add_admin_menu(){
	add_menu_page('Header & Footer scripts', 'Instan Scripts Settings', 'manage_options', 'instantsearch-admin-menu', 'instantsearch_scripts_page','',200);
}

function instantsearch_scripts_page(){
 
 	if(array_key_exists("api_key", $_POST)){
 		update_option('instantsearch_api_key',$_POST['api_key']);
 		?>
 			<div id="setting-error-settings-updated" class="updated settings-error notice is-dismissible"><strong>Settings have been saved.</strong></div>
 	    <?php
 	}

	$api_key = get_option('instantsearch_api_key', 'none');
	?>
	<div class="wrap">
	<form method="post" action="">
		<h2> Instan Search Settings </h2>
		<lable for"api_key"> Add Instant Search API key </lable>
		<textarea name="api_key"  class="large-text"> <?php print $api_key ?> </textarea>
		<input type="submit" name="submit_api_key" value="Update API Key" />
	</form>

	<?php
}


?>