 <?php
/*
Plugin Name:  Instant Search
Plugin URI:   https://developer.wordpress.org/plugins/the-basics/
Description:  Add instant search to your page.
Version:      1.0.0.
Author:       Humayun Babar
Author URI:   localhost
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  wporg
Domain Path:  /languages
*/

//load style and js
require_once(plugin_dir_path(__FILE__).'/is_init.php');
require_once(plugin_dir_path(__FILE__).'/instant_search-class.php');
add_shortcode('instantsearch', 'add_instant_search');

add_action('admin_menu','add_admin_menu');
