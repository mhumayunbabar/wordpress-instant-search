<?php

function is_add_scripts(){
?>
<link rel="stylesheet" href='https://cdn.jsdelivr.net/bootstrap/3.3.5/css/bootstrap.min.css'>
<link rel="stylesheet" href='https://cdn.jsdelivr.net/npm/instantsearch.js@1/dist/instantsearch.min.css'>
<link rel="stylesheet" href='<?php echo (plugins_url().'/instant_search/css/style.css') ?>'>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="<?php echo (plugins_url().'/instant_search/js/instantsearch.js') ?>"></script>
<script src="<?php echo (plugins_url().'/instant_search/js/main.js') ?>"></script>


<?php
}
